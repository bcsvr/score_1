import {
	getToken,
} from '@/utils/auth.js';


// 后台地址
export const base_url='http://192.168.0.102:8086'



//不加token的接口列表
const whiteList = [
	'/jeecg-boot/weChat/getInfo',
	'/jeecg-boot/points/propertyPointBanner/list/mp'
];



export default function request({
	url,
	data,
	method
}) {
	data = data || {};
	data.column = 'createTime';
	data.order = 'desc';
	return new Promise((resolve, reject) => {
		let request_obj = null
		let header = {
			'content-type': 'application/json',
		}
		//过滤whiteList中的接口不加token
		let flag = true
		flag = !(whiteList.includes(url))
		if(flag){
			if (getToken()) {
				header = Object.assign(header, {
					'X-Access-Token': getToken()
				})
			}
		}
		let request_base_param = {
			url: base_url + url,
			header,
			method,
			data,
			timeout: 100000,
			success: res => {
				const {statusCode} = res
				if (statusCode === 200) {
					resolve(res.data);
				} else {
					if(res.data.message.includes('ken失效')) {
						uni.showModal({
							showCancel: false,
							content: '登录已失效',
							title: '提示',
							confirmColor: '#e4181a',
							success(res) {
								if(res.confirm) {
									uni.clearStorageSync();
									uni.reLaunch({
										url: '/pages/login/index'
									})
								}
							}
						})
					};
					reject();
				}
			},
			fail: (err) => { //失败操作
				uni.showToast({
					icon: 'none',
					title: "网络开小差了~"
				})
				reject(err)
			}
		}
		uni.request(request_base_param)
	})
}
