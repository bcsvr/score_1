# points

#### 介绍
积分兑换商城系统，基于Java + SpringBoot + Mybatis Plus + Redis + Vue + antdv，支持积分商城、积分兑换等功能，包含小程序，系统管理后台。

#### 快捷体验chatGPT

<img src="https://foruda.gitee.com/images/1680944367241607254/eb18ac41_2042292.jpeg" width="200px" />
<img src="https://foruda.gitee.com/images/1681220540081279787/4fb27304_2042292.jpeg" width="200px" />

#### 演示地址

如需体验演示系统和试用小程序, 请添加下方微信

#### 联系方式
![输入图片说明](https://images.gitee.com/uploads/images/2022/0714/184028_03f76501_2042292.jpeg "28111657795185_.pic.jpg")

#### 演示图

![输入图片说明](https://foruda.gitee.com/images/1666873825905697112/7e028117_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1666873864552884468/45877eb6_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1666873908946395990/c6b2ff20_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1666873933623244049/63dd2192_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1666873951815615704/22f2ae2e_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1670639565421080107/a1cc2a0c_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1666873999987345378/6a6f661d_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1670639638694779497/10809849_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1670639689370781160/d65fec6e_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1670639817921787539/017f568c_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1670639741031103829/5c116710_2042292.png)

